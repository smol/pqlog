# Don't build a static binary, the duckdb libraries need NSS for name
# resolution and such anyway.
FROM golang:1.22 AS build
ADD . /src
WORKDIR /src
RUN go build -trimpath -o /pqlogd ./cmd/pqlogd

FROM registry.git.autistici.org/pipelines/images/base/debian:bookworm
COPY --from=build /pqlogd /usr/bin/pqlogd

ENTRYPOINT ["/usr/bin/pqlogd"]
