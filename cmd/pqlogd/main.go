package main

import (
	"context"
	"crypto/tls"
	"flag"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	pqhttp "git.autistici.org/smol/pqlog/http"
	"git.autistici.org/smol/pqlog/writer"
	"git.autistici.org/smol/pqlog/writer/pathgen"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	addr             = flag.String("addr", ":2944", "address to listen on")
	storageDir       = flag.String("dir", "/data/logs", "storage root dir")
	stateFile        = flag.String("state-file", "", "state file for the schema guesser")
	readTimeout      = flag.Duration("read-timeout", 30*time.Second, "HTTP read timeout")
	rotationInterval = flag.Duration("rotation-interval", 1*time.Hour, "log rotation interval")
	enableQuery      = flag.Bool("enable-query", false, "enable query handler")
	enableIngest     = flag.Bool("enable-ingest", false, "enable ingestion handler")
)

func main() {
	log.SetFlags(0)
	defaultsFromEnv(flag.CommandLine)
	flag.Parse()

	if !*enableQuery && !*enableIngest {
		log.Fatal("Must specify at least one of --enable-query or --enable-ingest")
	}

	uri, err := writer.ParseRemoteURI(*storageDir)
	if err != nil {
		log.Fatal(err)
	}

	mux := http.NewServeMux()
	mux.Handle("/metrics", promhttp.Handler())
	mux.HandleFunc("/health", livenessHandler)

	src, err := uri.Source()
	if err != nil {
		log.Fatal(err)
	}

	if *enableIngest {
		if err := os.MkdirAll(*storageDir, 0700); err != nil {
			log.Fatal(err)
		}

		pathGen, err := pathgen.New(filepath.Join(uri.Path, "logs"))
		if err != nil {
			log.Fatal(err)
		}

		w := writer.NewRotating(src, pathGen, *rotationInterval)
		defer w.Close()

		isrv := pqhttp.NewIngest(w, *stateFile)
		mux.Handle("/ingest", instrumentedHandler(
			http.HandlerFunc(isrv.IngestHandler),
			"ingest",
			true))
		mux.HandleFunc("/schema", isrv.SchemaHandler)
		mux.HandleFunc("/debug/schema", isrv.DebugHandler)
	}

	if *enableQuery {
		qsrv, err := pqhttp.NewQuery(src, "logs.*.pq")
		if err != nil {
			log.Fatal(err)
		}
		defer qsrv.Close()
		mux.Handle("/query", instrumentedHandler(
			qsrv, "query", false))
	}

	httpSrv := &http.Server{
		Handler:           mux,
		ReadTimeout:       *readTimeout,
		ReadHeaderTimeout: 5 * time.Second,
		WriteTimeout:      10 * time.Second,
		IdleTimeout:       600 * time.Second,
	}
	l, err := buildListener()
	if err != nil {
		log.Fatal(err)
	}

	// Set up a signal handler for safe shutdown, so that
	// writer.Close() will be invoked and we won't create
	// incomplete (partial) output files.
	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		log.Printf("received termination signal, exiting...")
		ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		httpSrv.Shutdown(ctx) // nolint:errcheck
		cancel()
		httpSrv.Close()
	}()
	signal.Notify(sigCh, syscall.SIGTERM, syscall.SIGINT)

	log.Printf("pqlogd listening on %s", *addr)
	if err := httpSrv.Serve(l); err != nil && err != http.ErrServerClosed {
		log.Fatal(err)
	}
}

func buildListener() (net.Listener, error) {
	l, err := net.Listen("tcp", *addr)
	if err != nil {
		return nil, err
	}

	tlsConfig, err := buildTLSConfig()
	if err != nil {
		return nil, err
	}
	if tlsConfig != nil {
		l = tls.NewListener(l, tlsConfig)
	}
	return l, nil
}

func livenessHandler(w http.ResponseWriter, _ *http.Request) {
	io.WriteString(w, "OK") // nolint:errcheck
}

// Instrumentation.
func instrumentedHandler(h http.Handler, name string, withRequestSize bool) http.Handler {
	labels := prometheus.Labels{"handler": name}
	if withRequestSize {
		h = promhttp.InstrumentHandlerRequestSize(
			requestSize, h)
	}
	return promhttp.InstrumentHandlerInFlight(
		inFlightRequests,
		promhttp.InstrumentHandlerCounter(
			numRequests.MustCurryWith(labels),
			h))
}

var (
	numRequests = promauto.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_requests_total",
			Help: "Total number of requests.",
		},
		[]string{"handler", "code"},
	)
	inFlightRequests = promauto.NewGauge(
		prometheus.GaugeOpts{
			Name: "http_requests_inflight",
			Help: "Number of in-flight requests.",
		},
	)
	requestSize = promauto.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "http_request_size_bytes",
			Help:    "Histogram of request sizes.",
			Buckets: []float64{1000, 10000, 100000, 1000000, 10000000},
		},
		[]string{},
	)
)
