package main

import (
	"crypto/tls"
	"crypto/x509"
	"flag"
	"fmt"
	"os"
)

var (
	tlsCert = flag.String("tls-cert", "", "TLS certificate")
	tlsKey  = flag.String("tls-key", "", "TLS private key")
	tlsCA   = flag.String("tls-ca", "", "TLS CA certificate")
)

// buildTLSConfig returns a tls.Config built from command-line options.
func buildTLSConfig() (*tls.Config, error) {
	if *tlsCert == "" || *tlsKey == "" {
		return nil, nil
	}

	cert, err := tls.LoadX509KeyPair(*tlsCert, *tlsKey)
	if err != nil {
		return nil, err
	}

	config := &tls.Config{
		Certificates:             []tls.Certificate{cert},
		MinVersion:               tls.VersionTLS12,
		PreferServerCipherSuites: true,
		NextProtos:               []string{"h2", "http/1.1"},
	}

	if *tlsCA != "" {
		cas, err := loadCA(*tlsCA)
		if err != nil {
			return nil, err
		}
		config.ClientAuth = tls.RequireAndVerifyClientCert
		config.ClientCAs = cas
	}

	return config, nil
}

// loadCA loads a file containing CA certificates into a x509.CertPool.
func loadCA(path string) (*x509.CertPool, error) {
	data, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}
	cas := x509.NewCertPool()
	if !cas.AppendCertsFromPEM(data) {
		return nil, fmt.Errorf("no certificates could be parsed in %s", path)
	}
	return cas, nil
}
