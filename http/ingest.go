package pqhttp

import (
	"bufio"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"sync"

	"git.autistici.org/smol/pqlog/schema"
	"git.autistici.org/smol/pqlog/writer"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

type IngestServer struct {
	guesser   *schema.Guesser
	schema    *schema.Schema
	pschema   *schema.ParquetSchemaHandler
	writer    writer.Writer
	stateFile string

	// Ingestion mutex. Ingestion requests are single-threaded, we
	// rely on the http.Server timeouts to expire connections that
	// hold onto the lock for too long.
	mx sync.Mutex
}

func NewIngest(w writer.Writer, stateFile string) *IngestServer {
	var g *schema.Guesser
	if stateFile != "" {
		var err error
		g, err = schema.LoadGuesser(stateFile)
		if err != nil {
			log.Printf("warning: loading schema guesser state file: %v", err)
			g = schema.NewGuesser()
		}
	} else {
		g = schema.NewGuesser()
	}
	return &IngestServer{
		guesser:   g,
		stateFile: stateFile,
		writer:    w,
	}
}

/** one-line-at-a-time version, unused.
func (s *IngestServer) ingest(r io.Reader) error {
	s.mx.Lock()
	s.mx.Unlock()

	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		s.numRecords++
		tuples, unprocessed, err := schema.RecordToTuples(scanner.Bytes())
		if err != nil {
			continue
		}
		changed := s.guesser.ProcessRecord(tuples, unprocessed)
		if changed {
			s.numSchemaChanges++
			s.schema = s.guesser.NewSchema()
			s.pschema = s.schema.ParquetSchemaHandler()
			if err := s.writer.Reopen(s.pschema.SchemaHandler); err != nil {
				return err
			}
		}
		b := s.pschema.TuplesToRow(tuples)
		if err := s.writer.Write(b); err != nil {
			return err
		}
	}
	return scanner.Err()
}
**/

func (s *IngestServer) ingestBatch(r io.Reader) (int, error) {
	s.mx.Lock()
	defer s.mx.Unlock()

	var tupleMtx [][]schema.Tuple
	changed := false

	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		tuples, unprocessed, err := schema.RecordToTuples(scanner.Bytes())
		if err != nil {
			log.Printf("input error: %v", err)
			continue
		}
		recChanged := s.guesser.ProcessRecord(tuples, unprocessed)
		if recChanged {
			changed = true
		}
		tupleMtx = append(tupleMtx, tuples)
	}

	if changed {
		numSchemaChanges.Inc()
		s.schema = s.guesser.NewSchema()
		s.pschema = s.schema.ParquetSchemaHandler()
		if err := s.writer.Reopen(s.pschema); err != nil {
			return 0, err
		}
		if s.stateFile != "" {
			if err := s.guesser.Save(s.stateFile); err != nil {
				log.Printf("warning: failed to save schema guesser state: %v", err)
			}
		}
	}

	nFields, nErrs := s.guesser.Metrics()
	numSchemaFields.Set(float64(nFields))
	numSchemaErrors.Set(float64(nErrs))

	for _, tuples := range tupleMtx {
		b := s.pschema.TuplesToRow(tuples)
		if err := s.writer.Write(b); err != nil {
			return 0, err
		}
	}

	return len(tupleMtx), scanner.Err()
}

func (s *IngestServer) IngestHandler(w http.ResponseWriter, req *http.Request) {
	if req.Method != "POST" {
		http.Error(w, "Bad method", http.StatusMethodNotAllowed)
		return
	}

	n, err := s.ingestBatch(req.Body)
	if err != nil {
		log.Printf("ingestion error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		numIngestOps.WithLabelValues("error").Inc()
		return
	}
	numRecords.Add(float64(n))
	numIngestOps.WithLabelValues("ok").Inc()

	w.WriteHeader(http.StatusAccepted)
}

func (s *IngestServer) DebugHandler(w http.ResponseWriter, req *http.Request) {
	s.mx.Lock()
	out := s.guesser.Debug()
	s.mx.Unlock()

	w.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode(out)
}

func (s *IngestServer) SchemaHandler(w http.ResponseWriter, req *http.Request) {
	s.mx.Lock()
	var out *schema.SchemaDebugInfo
	if s.schema != nil {
		out = s.schema.Debug()
	}
	s.mx.Unlock()

	w.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode(out)
}

// Instrumentation.
var (
	numRecords = promauto.NewCounter(prometheus.CounterOpts{
		Name: "pqlog_records_processed",
		Help: "Counter of processed records.",
	})
	numSchemaChanges = promauto.NewCounter(prometheus.CounterOpts{
		Name: "pqlog_schema_changes",
		Help: "How many times the schema was adapted for changes.",
	})
	numSchemaFields = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "pqlog_schema_fields_count",
		Help: "Number of fields in the schema.",
	})
	numSchemaErrors = promauto.NewGauge(prometheus.GaugeOpts{
		Name: "pqlog_schema_errors",
		Help: "Number of unprocessable fields detected in the input.",
	})
	numIngestOps = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "pqlog_ingest_ops",
		Help: "Number of ingest operations.",
	}, []string{"status"})
)
