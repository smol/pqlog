package pqhttp

import (
	"bufio"
	"context"
	"database/sql"
	"database/sql/driver"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/http"
	"reflect"
	"strings"

	"git.autistici.org/smol/pqlog/writer"
	"github.com/marcboeker/go-duckdb"
)

type queryError struct {
	error
}

func newQueryError(err error) *queryError {
	return &queryError{error: err}
}

type QueryServer struct {
	glob string
	db   *sql.DB
	src  writer.Source
}

func NewQuery(src writer.Source, pattern string) (*QueryServer, error) {
	connector, err := duckdb.NewConnector("", func(execer driver.ExecerContext) error {
		stmts, err := src.DatabaseInit()
		if err != nil {
			return err
		}
		for _, stmt := range stmts {
			if _, err := execer.ExecContext(context.Background(), stmt, nil); err != nil {
				return err
			}
		}
		return nil
	})
	if err != nil {
		return nil, err
	}

	db := sql.OpenDB(connector)
	return &QueryServer{
		glob: pattern,
		db:   db,
		src:  src,
	}, nil
}

func (q *QueryServer) Close() {
	q.db.Close()
}

func (q *QueryServer) runQuery(ctx context.Context, w io.Writer, query string) error {
	table := fmt.Sprintf("logs%08x", rand.Int63())
	query = strings.Replace(query, "$table", table, -1)

	tx, err := q.db.BeginTx(ctx, nil)
	if err != nil {
		return fmt.Errorf("failed to create transaction: %w", err)
	}
	defer tx.Rollback() // nolint:errcheck

	remoteGlob := q.src.ParquetURI(q.glob)
	_, err = tx.Exec(fmt.Sprintf("CREATE VIEW %s AS SELECT * FROM read_parquet('%s', union_by_name=true)", table, remoteGlob))
	if err != nil {
		return fmt.Errorf("failed to create DuckDB view: %w", err)
	}
	defer func() {
		// Well, ok, we have the Rollback() but.
		// nolint:errcheck
		tx.Exec(fmt.Sprintf("DROP VIEW %s", table))
	}()

	// Yeah we're just straight up executing the POST SQL query as is.
	rows, err := tx.Query(query)
	if err != nil {
		// Report this as a client-originated error.
		return newQueryError(fmt.Errorf("query execution error: %w", err))
	}
	defer rows.Close()

	bw := bufio.NewWriter(w)

	_, _ = io.WriteString(bw, "{\"status\":\"ok\",\"results\":[")
	cols, _ := rows.ColumnTypes()
	first := true
	for rows.Next() {
		if first {
			first = false
		} else {
			_, _ = io.WriteString(bw, ",")
		}

		values := make([]any, len(cols))
		for i := 0; i < len(cols); i++ {
			value := reflect.Zero(cols[i].ScanType()).Interface()
			values[i] = &value
		}
		if err := rows.Scan(values...); err != nil {
			return fmt.Errorf("Scan error: %w", err)
		}
		row := make(map[string]any, len(cols))
		for i := 0; i < len(cols); i++ {
			row[cols[i].Name()] = values[i]
		}
		if err := json.NewEncoder(bw).Encode(row); err != nil {
			return fmt.Errorf("JSON encode error: %w", err)
		}
	}
	_, _ = io.WriteString(bw, "]}\n")

	bw.Flush()

	return rows.Err()
}

func (q *QueryServer) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	if req.Method != "POST" {
		http.Error(w, "Bad method", http.StatusMethodNotAllowed)
		return
	}

	query := req.FormValue("q")
	if query == "" || !strings.Contains(query, "$table") {
		http.Error(w, "Empty or malformed query", http.StatusBadRequest)
		return
	}

	w.Header().Set("Cache-Control", "no-store")
	w.Header().Set("Content-Type", "application/json")

	if err := q.runQuery(req.Context(), w, query); err != nil {
		status := http.StatusInternalServerError
		if errors.Is(err, &queryError{}) {
			status = http.StatusBadRequest
		}
		log.Printf("query error: %v", err)
		http.Error(w, err.Error(), status)
	}
}
