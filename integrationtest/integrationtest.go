package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"path/filepath"
	"runtime"
	"runtime/pprof"
	"strings"
	"time"

	pqhttp "git.autistici.org/smol/pqlog/http"
	"git.autistici.org/smol/pqlog/writer"
	"git.autistici.org/smol/pqlog/writer/pathgen"
)

var (
	cpuprofile   = flag.String("cpuprofile", "cpu.prof", "cpu profile")
	heapprofile  = flag.String("heapprofile", "heap.prof", "memory heap profile")
	allocprofile = flag.String("allocprofile", "alloc.prof", "memory allocations profile")
)

type testRecord struct {
	Timestamp time.Time `json:"@timestamp"`
	Data      struct {
		Tags    []string `json:"tag,omitempty"`
		Process string   `json:"process,omitempty"`
		Name    string   `json:"name,omitempty"`
		Uid     int      `json:"uid,omitempty"`
	} `json:"data,omitempty"`
	Host    string  `json:"host"`
	A0      int64   `json:"a0"`
	A1      float64 `json:"a1"`
	Message string  `json:"message,omitempty"`
}

var (
	testHosts = []string{"host1", "host2", "host3", "host4", "host5", "host6", "host7", "host8", "host9"}
	testTags  = []string{"tag1", "tag2", "tag3", "tag4"}
)

func randomRecord(rec *testRecord) {
	rec.Timestamp = time.Unix(rand.Int63n(time.Now().Unix()), 0)
	rec.Host = testHosts[rand.Intn(len(testHosts))]

	if rand.Float64() > 0.8 {
		n := rand.Intn(16)
		rec.Data.Name = fmt.Sprintf("user%d", n)
		rec.Data.Uid = n
	}

	if rand.Float64() > 0.9 {
		rec.Data.Tags = nil
		numTags := 1 + rand.Intn(3)
		if numTags > len(testTags) {
			numTags = len(testTags)
		}
		perm := rand.Perm(len(testTags))[:numTags]
		for _, i := range perm {
			rec.Data.Tags = append(rec.Data.Tags, testTags[i])
		}
	}

	if rand.Float64() > 0.98 {
		rec.Data.Process = fmt.Sprintf("splat%04d", rand.Intn(4096))
	}

	if rand.Float64() > 0.9995 {
		rec.A0 = rand.Int63n(3)
		rec.A1 = rand.Float64() * 128.0
	} else {
		rec.Message = "this is a test message, quite boring"
	}
}

func genRandomRecords(dir string, tot int64) {
	var n int64 = 0
	fc := 0
	var rec testRecord

	for n < tot {
		f, _ := os.Create(filepath.Join(dir, fmt.Sprintf("rec%08d.ndjson", fc)))
		next := n + 10000
		for n < next {
			randomRecord(&rec)
			json.NewEncoder(f).Encode(&rec)
			n++
		}
		f.Close()
		fc++
	}
}

func runBench(f func(string) error) error {
	dir, err := os.MkdirTemp("", "")
	if err != nil {
		return err
	}
	defer os.RemoveAll(dir)

	pathGen, err := pathgen.New(filepath.Join(dir, "logs"))
	if err != nil {
		return fmt.Errorf("pathgen.New: %w", err)
	}

	uri := &url.URL{Path: dir}
	src := writer.NewLocalSource(uri)
	w := writer.NewRotating(src, pathGen, 1*time.Hour)
	defer w.Close()

	isrv := pqhttp.NewIngest(w, "")
	mux := http.NewServeMux()
	mux.HandleFunc("/ingest", isrv.IngestHandler)

	qsrv, err := pqhttp.NewQuery(src, "logs.*.pq")
	if err != nil {
		return err
	}
	mux.Handle("/query", qsrv)

	httpSrv := httptest.NewServer(mux)
	defer httpSrv.Close()

	return f(httpSrv.URL)
}

var client = new(http.Client)

func loadFile(uri, path string) error {
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()

	req, err := http.NewRequest("POST", uri+"/ingest", f)
	if err != nil {
		return err
	}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		return fmt.Errorf("HTTP status %d", resp.StatusCode)
	}
	return nil
}

func loadDir(uri, dir string) error {
	files, _ := os.ReadDir(dir)
	for _, f := range files {
		if err := loadFile(uri, filepath.Join(dir, f.Name())); err != nil {
			return fmt.Errorf("%s: %w", f.Name(), err)
		}
	}
	return nil
}

type queryResponse struct {
	Status  string           `json:"status"`
	Results []map[string]any `json:"results"`
}

func makeQuery(uri string) error {
	c := new(http.Client)
	values := make(url.Values)
	values.Set("q", "SELECT COUNT(*) AS c, \"data.name\" FROM $table GROUP BY \"data.name\" ORDER BY c DESC")
	req, err := http.NewRequest("POST", uri+"/query", strings.NewReader(values.Encode()))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	resp, err := c.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("HTTP status %d", resp.StatusCode)
	}

	var qresp queryResponse
	if err := json.NewDecoder(resp.Body).Decode(&qresp); err != nil {
		return err
	}

	if qresp.Status != "ok" {
		return fmt.Errorf("response.status is '%s', expected 'ok'", qresp.Status)
	}
	if len(qresp.Results) == 0 {
		return errors.New("response.results is empty")
	}

	return nil
}

func runIntegrationTest(profile bool) error {
	dir, err := os.MkdirTemp("", "")
	if err != nil {
		return err
	}
	defer os.RemoveAll(dir)

	// When loading the database with test data, lower the max
	// file size so as to ensure a few finalized Parquet files are
	// created and we can query them right away.
	writer.MaxRecordsPerFile = 10000
	log.Printf("generating random records")
	genRandomRecords(dir, 10*writer.MaxRecordsPerFile)

	log.Printf("starting benchmark")
	return runBench(func(uri string) error {
		if profile {
			f, err := os.Create(*cpuprofile)
			if err != nil {
				return err
			}
			pprof.StartCPUProfile(f)
		}

		if err := loadDir(uri, dir); err != nil {
			return err
		}

		if profile {
			// Stop collecting profiles.
			pprof.StopCPUProfile()
			runtime.GC()
			dumpMemProfile("heap", *heapprofile)
			dumpMemProfile("allocs", *allocprofile)
		}

		// Run validation query.
		if err := makeQuery(uri); err != nil {
			return fmt.Errorf("validation query failed: %w", err)
		}
		log.Printf("validation query succeeded")

		return nil
	})
}

func dumpMemProfile(kind, filename string) {
	f, err := os.Create(filename)
	if err != nil {
		log.Fatal("could not create memory profile: ", err)
	}
	defer f.Close()
	pprof.Lookup(kind).WriteTo(f, 0)
}

func main() {
	flag.Parse()

	if err := runIntegrationTest(true); err != nil {
		log.Fatal(err)
	}
}
