package main

import (
	"testing"
)

func TestIntegration(t *testing.T) {
	if err := runIntegrationTest(false); err != nil {
		t.Fatal(err)
	}
}
