package schema

import (
	"fmt"

	"github.com/xitongsys/parquet-go/common"
	"github.com/xitongsys/parquet-go/parquet"
	pqschema "github.com/xitongsys/parquet-go/schema"
)

// Generate a new Parquet tag for a field of our schema.
func parquetTag(key string, t Type) *common.Tag {
	info := common.NewTag()
	info.InName = common.StringToVariableName(key)
	info.ExName = key
	info.RepetitionType = parquet.FieldRepetitionType_OPTIONAL
	switch t {
	case TypeNumber:
		info.Type = "DOUBLE"
	case TypeTimestamp:
		info.Type = "INT64"
		info.ConvertedType = "TIMESTAMP_MICROS"
	default:
		info.Type = "BYTE_ARRAY"
		info.ConvertedType = "UTF8"
		if t == TypeList {
			info.RepetitionType = parquet.FieldRepetitionType_REPEATED
		}
	}
	return info
}

// ParquetSchemaHandler is a parquet SchemaHandler wrapper that
// describes our "flat" schema. It also provides a TuplesToRow()
// method to convert input tuples into a matrix format which is easier
// to marshal in Parquet format.
type ParquetSchemaHandler struct {
	*pqschema.SchemaHandler
	numFields     int
	fieldIndexMap map[string]int
	upcast        map[string]upcastFunc
	zeros         []any
}

// Generate a Parquet schema (wrapped by a parquetSchemaHandler).
func (s *Schema) ParquetSchemaHandler() *ParquetSchemaHandler {
	var schemaList []*parquet.SchemaElement
	var infos []*common.Tag
	fieldIndexMap := make(map[string]int)

	rootSchema := parquet.NewSchemaElement()
	rootSchema.Name = "Parquet_go_root"
	rootNumChildren := int32(len(s.columns))
	rootSchema.NumChildren = &rootNumChildren
	rt := parquet.FieldRepetitionType_REQUIRED
	rootSchema.RepetitionType = &rt
	schemaList = append(schemaList, rootSchema)

	rootInfo := common.NewTag()
	rootInfo.InName = "Parquet_go_root"
	rootInfo.ExName = "parquet_go_root"
	rootInfo.RepetitionType = parquet.FieldRepetitionType_REQUIRED
	infos = append(infos, rootInfo)

	idx := 0
	var zeros []any
	for key, col := range s.columns {
		info := parquetTag(key, col)
		infos = append(infos, info)
		el, err := common.NewSchemaElementFromTagMap(info)
		if err != nil {
			panic(fmt.Sprintf("failed to create schema from tag map: %s", err.Error()))
		}
		schemaList = append(schemaList, el)
		zeros = append(zeros, col.ZeroValue())
		fieldIndexMap[key] = idx
		idx++
	}

	res := pqschema.NewSchemaHandlerFromSchemaList(schemaList)
	res.Infos = infos
	res.CreateInExMap()

	return &ParquetSchemaHandler{
		SchemaHandler: res,
		numFields:     idx,
		fieldIndexMap: fieldIndexMap,
		zeros:         zeros,
		upcast:        s.upcast,
	}
}

func (s *ParquetSchemaHandler) TuplesToRow(tuples []Tuple) []any {
	row := make([]any, s.numFields)
	//copy(row, s.zeros)
	for _, t := range tuples {
		idx := s.fieldIndexMap[t.key]
		value := t.value
		if u := s.upcast[t.key]; u != nil {
			value = u(value)
		}
		row[idx] = value
	}
	return row
}

func (s *ParquetSchemaHandler) Zero(i int) any {
	return s.zeros[i]
}
