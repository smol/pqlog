package schema

import (
	"encoding/json"
	"fmt"
	"os"
	"regexp"
)

const keySeparator = "."

// Type of a field.
type Type int

// Fundamental data types of our schema. Just basic datatypes, plus
// 'list' which is just a repeated string element. Type identifiers
// are individual bits so they can be OR'ed together.
const (
	TypeString = 1 << iota
	TypeNumber
	TypeTimestamp
	TypeList
)

const maxTypeBits = 8 // Just not smaller than the number of types.

var isoTimestampRx = regexp.MustCompile(`^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}`)

func isTimestamp(s string) bool {
	return isoTimestampRx.MatchString(s)
}

func (t Type) String() string {
	switch t {
	case TypeString:
		return "STRING"
	case TypeNumber:
		return "NUMBER"
	case TypeTimestamp:
		return "TIMESTAMP"
	case TypeList:
		return "LIST"
	default:
		return "UNKNOWN"
	}
}

// Return the zero value for the specific type.
func (t Type) ZeroValue() any {
	switch t {
	case TypeString:
		return ""
	case TypeNumber:
		return 0.0
	case TypeTimestamp:
		return int64(0)
	case TypeList:
		return ""
	default:
		return nil
	}
}

type upcastFunc func(any) any

func upcastToString(value any) any {
	switch value := value.(type) {
	case string:
		return value
	case float64:
		return fmt.Sprintf("%g", value)
	default:
		panic("wrong arg to upcastToString")
	}
}

func upcastToList(value any) any {
	switch value := value.(type) {
	case string:
		return []any{value}
	case float64:
		return []any{fmt.Sprintf("%g", value)}
	default:
		// Something will panic later on.
		return value
	}
}

func (t typeGuesser) supportedUpcast() (Type, upcastFunc, bool) {
	switch t {
	// Scalars upcasts to string.
	case TypeString | TypeNumber, TypeString | TypeTimestamp:
		return TypeString, upcastToString, true

	// List/scalar combinations upcast to list.
	case TypeString | TypeList, TypeNumber | TypeList:
		// Upcast scalars to lists.
		return TypeList, upcastToList, true

	default:
		return 0, nil, false
	}
}

// A 'type guesser' keeps track of data types seen for a specific
// column. The 'upcast' functionality can reconcile some cases where
// multiple data types are seen (usually by casting to string).
// The implementation is just a bitset.
type typeGuesser int

func (g typeGuesser) isSet(t Type) bool {
	return (int(g) & int(t)) == int(t)
}

func (g typeGuesser) set(t Type) typeGuesser {
	return typeGuesser(int(g) | int(t))
}

// Return the 'best' data type for this column. The boolean value will
// be false if there is a data type conflict and no 'upcast' is
// possible.
func (g typeGuesser) datatype() (Type, upcastFunc, bool) {
	var n int
	var t Type
	for i := 0; i < maxTypeBits; i++ {
		j := Type(1 << i)
		if g.isSet(j) {
			n++
			t = j
		}
	}
	if n > 1 {
		return g.supportedUpcast()
	}
	return t, nil, true
}

// The Guesser keeps track of the columns we see (after
// flattening the input) and their associated data types, and infers a
// schema over time, to the extent where it is possible to do so.
// Conflicts are tracked in the 'errors' map.
type Guesser struct {
	columns        map[string]typeGuesser
	errors         map[string]string
	versionCounter int64
}

func NewGuesser() *Guesser {
	return &Guesser{
		columns: make(map[string]typeGuesser),
		errors:  make(map[string]string),
	}
}

// Metrics is an internal endpoint to obtain exportable metrics about
// the internal state of the Guesser, while holding an external lock.
func (g *Guesser) Metrics() (int, int) {
	return len(g.columns), len(g.errors)
}

type serializableGuesser struct {
	Columns        map[string]int    `json:"columns"`
	Errors         map[string]string `json:"errors"`
	VersionCounter int64             `json:"version_counter"`
}

func (sg *serializableGuesser) Guesser() *Guesser {
	g := &Guesser{
		columns:        make(map[string]typeGuesser),
		errors:         sg.Errors,
		versionCounter: sg.VersionCounter,
	}
	for name, col := range sg.Columns {
		g.columns[name] = typeGuesser(col)
	}
	return g
}

func LoadGuesser(path string) (*Guesser, error) {
	var sg serializableGuesser
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	if err := json.NewDecoder(f).Decode(&sg); err != nil {
		return nil, err
	}
	return sg.Guesser(), nil
}

func (g *Guesser) Save(path string) error {
	f, err := os.Create(path)
	if err != nil {
		return err
	}
	defer f.Close()
	sg := serializableGuesser{
		Columns:        make(map[string]int),
		Errors:         g.errors,
		VersionCounter: g.versionCounter,
	}
	for name, col := range g.columns {
		sg.Columns[name] = int(col)
	}
	return json.NewEncoder(f).Encode(&sg)
}

// This structure holds the inferred schema of the datastream at a
// specific point in time.
type Schema struct {
	Version int64
	columns map[string]Type
	upcast  map[string]upcastFunc
}

// NewSchema returns the best possible version of a schema given our
// current data type guesses.
func (g *Guesser) NewSchema() *Schema {
	cols := make(map[string]Type)
	upcast := make(map[string]upcastFunc)
	for name, col := range g.columns {
		if t, upcastFn, ok := col.datatype(); ok {
			cols[name] = t
			upcast[name] = upcastFn
		}
	}
	g.versionCounter++
	return &Schema{
		Version: g.versionCounter,
		columns: cols,
		upcast:  upcast,
	}
}

// ProcessRecord analyzes a new incoming record (tuples list), and
// keep track of schema processing errors. Return true if the schema
// has changed after processing the record, having learned something
// new.
func (g *Guesser) ProcessRecord(tuples []Tuple, unprocessed []TypeError) bool {
	for _, u := range unprocessed {
		g.errors[u.key] = u.err
	}

	changed := false
	for _, t := range tuples {
		mask := g.columns[t.key]
		newMask := mask.set(t.valueType)
		g.columns[t.key] = newMask
		if mask != newMask {
			changed = true
		}
	}

	return changed
}
