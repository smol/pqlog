package schema

import "fmt"

func (g typeGuesser) debugState() ColumnDebugState {
	var n int
	var ds ColumnDebugState
	for i := 0; i < maxTypeBits; i++ {
		j := Type(1 << i)
		if g.isSet(j) {
			n++
			ds.Types = append(ds.Types, j.String())
		}
	}
	if n > 1 {
		s := "type conflict"
		if up, _, ok := g.supportedUpcast(); ok {
			s = fmt.Sprintf("upcast to %s", up.String())
		}
		ds.Notes = s
	}
	return ds
}

// ColumnDebugState holds information about a specific column.
type ColumnDebugState struct {
	Types []string `json:"types"`
	Notes string   `json:"notes,omitempty"`
}

// GuesserDebugState represents the state of the Guesser in human-readable form.
type GuesserDebugState struct {
	Columns map[string]ColumnDebugState `json:"fields"`
	Errors  map[string]string           `json:"errors,omitempty"`
}

// Debug returns information on the Guesser internals.
func (g *Guesser) Debug() *GuesserDebugState {
	state := GuesserDebugState{
		Columns: make(map[string]ColumnDebugState),
		Errors:  make(map[string]string),
	}
	for name, col := range g.columns {
		state.Columns[name] = col.debugState()
	}
	for name, err := range g.errors {
		state.Errors[name] = err
	}
	return &state
}

type SchemaFieldDebugInfo struct {
	Name string `json:"name"`
	Type string `json:"type"`
}

type SchemaDebugInfo struct {
	Version int64                  `json:"version"`
	Fields  []SchemaFieldDebugInfo `json:"fields"`
}

// Debug returns a human-friendly representation of the schema.
func (s *Schema) Debug() *SchemaDebugInfo {
	var info SchemaDebugInfo
	info.Version = s.Version
	for name, col := range s.columns {
		info.Fields = append(info.Fields, SchemaFieldDebugInfo{
			Name: name,
			Type: col.String(),
		})
	}
	return &info
}
