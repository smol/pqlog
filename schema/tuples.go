package schema

import (
	"encoding/json"
	"fmt"
	"reflect"
	"strings"
	"time"
)

// Tuple is just a typed key/value pair.
type Tuple struct {
	key       string
	valueType Type
	value     any
}

// TypeError holds information about an unparseable column.
type TypeError struct {
	key string
	err string
}

// Verify that a list contains only compatible (scalar) elements.
func checkList(l []any) (Type, bool) {
	for _, item := range l {
		switch item.(type) {
		case float64, string:
		default:
			return 0, false
		}
	}
	return TypeList, true
}

// Flatten a generic map[string]any (deserialized JSON) into a list of
// tuples, along with any elements that are not compatible with our
// schema rules. This function is recursive.
func flatten(m map[string]any, pfx string, tuples []Tuple, unprocessed []TypeError) ([]Tuple, []TypeError) {
	for key, value := range m {
		var vtype Type
		switch tvalue := value.(type) {
		case map[string]any:
			tuples, unprocessed = flatten(tvalue, pfx+key+keySeparator, tuples, unprocessed)
			continue
		case []any:
			if ltype, ok := checkList(tvalue); ok {
				vtype = ltype
			} else {
				unprocessed = append(unprocessed, TypeError{
					key: pfx + key,
					err: "unsupported composite elements within list field",
				})
				continue
			}
		case float64:
			vtype = TypeNumber
		case string:
			vtype = TypeString
			if isTimestamp(tvalue) {
				if ts, err := time.Parse(time.RFC3339, tvalue); err == nil {
					vtype = TypeTimestamp
					value = ts.UnixNano() / 1000
				}
			}
		default:
			unprocessed = append(unprocessed, TypeError{
				key: pfx + key,
				err: fmt.Sprintf("unsupported value type %v", reflect.TypeOf(value)),
			})
			continue
		}
		tuples = append(tuples, Tuple{key: pfx + key, valueType: vtype, value: value})
	}
	return tuples, unprocessed
}

// unmarshalRecordRecursively can look into the special 'message'
// field and eventually unmarshal it twice.
func unmarshalRecordRecursively(record []byte) (map[string]any, error) {
	var m map[string]any
	if err := json.Unmarshal(record, &m); err != nil {
		return nil, err
	}

	// Try to unmarshal 'message' (or 'MESSAGE') if it looks like
	// JSON (possibly with an optional '@cee:' prefix).
	var message, fieldName string
	if s, ok := m["message"].(string); ok {
		fieldName = "message"
		message = s
	} else if s, ok := m["MESSAGE"].(string); ok {
		fieldName = "MESSAGE"
		message = s
	}

	if message != "" {
		message = strings.TrimPrefix(message, "@cee:")
		if strings.HasPrefix(message, "{") {
			if m2, err := unmarshalRecordRecursively([]byte(message)); err == nil {
				m[fieldName] = m2
			}
		}
	}

	return m, nil
}

// RecordToTuples converts a JSON record to tuples, by flattening the
// deserialized map.
func RecordToTuples(record []byte) ([]Tuple, []TypeError, error) {
	m, err := unmarshalRecordRecursively(record)
	if err != nil {
		return nil, nil, err
	}

	// Allocation optimization: assume the incoming record is
	// flat-ish and preallocate a Tuple slice.
	tupleBuf := make([]Tuple, 0, len(m))

	tuples, unprocessed := flatten(m, "", tupleBuf, nil)
	return tuples, unprocessed, nil
}
