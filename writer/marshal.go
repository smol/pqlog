package writer

import (
	"errors"
	"flag"
	"strings"

	"git.autistici.org/smol/pqlog/schema"
	"github.com/xitongsys/parquet-go/common"
	"github.com/xitongsys/parquet-go/layout"
	"github.com/xitongsys/parquet-go/parquet"
	pqschema "github.com/xitongsys/parquet-go/schema"
	"github.com/xitongsys/parquet-go/source"
	pqwriter "github.com/xitongsys/parquet-go/writer"
)

var (
	parquetCompression  = parquetCompressionFlag{codec: parquet.CompressionCodec_SNAPPY}
	parquetRowGroupSize = flag.Int("parquet-row-group-size", 32, "Parquet row group size (MB)")
	parquetPageSize     = flag.Int("parquet-page-size", 8192, "Parquet page size")
)

func init() {
	flag.Var(&parquetCompression, "parquet-compression", "Parquet compression `algorithm`")
}

type parquetCompressionFlag struct {
	codec parquet.CompressionCodec
}

func (p *parquetCompressionFlag) String() string {
	return p.codec.String()
}

func (p *parquetCompressionFlag) Set(value string) error {
	switch strings.ToLower(value) {
	case "none":
		p.codec = parquet.CompressionCodec_UNCOMPRESSED
	case "snappy":
		p.codec = parquet.CompressionCodec_SNAPPY
	case "gzip":
		p.codec = parquet.CompressionCodec_GZIP
	case "lz4":
		p.codec = parquet.CompressionCodec_LZ4
	case "zstd":
		p.codec = parquet.CompressionCodec_ZSTD
	default:
		return errors.New("unknown compression")
	}
	return nil
}

// Parquet marshaling code. Due to our peculiar data layout, there is
// nothing that immediately serves our purpose in the parquet-go
// library: the flattening of the data means that we can't simply feed
// the input JSON to parquet-go's JSON marshaler, and the presence of
// semi-structured data (TypeList repeated elements) is not supported
// by the CSV marshaler
//
// Instead, what we do is similar to the CSV marshaler (the simplest
// of the lot): we build a matrix of num_records x num_fields 'any'
// objects, then dump it column by column. We could, I guess,
// serialize our records to JSON again, but this approach seems to
// have a smaller performance impact.

// newTuplesWriter creates a new ParquetWriter that supports
// marshaling of the []any rows generated by
// parquetSchemaHandler.tuplesToRow().
//
// We can't just instantiate a new ParquetWriter with our
// SchemaHandler (and just set MarshalFunc later), because for some
// reason NewSchemaHandlerFromSchemaHandler (which gets called in that
// case) is not 'transparent' and returns something different than the
// original schema...
func newTuplesWriter(schemaHandler *schema.ParquetSchemaHandler, pfile source.ParquetFile, np int64) (*pqwriter.ParquetWriter, error) {
	res := new(pqwriter.ParquetWriter)
	res.SchemaHandler = schemaHandler.SchemaHandler
	res.PFile = pfile
	res.PageSize = int64(*parquetPageSize)
	res.RowGroupSize = int64(*parquetRowGroupSize) * 1024 * 1024
	res.CompressionType = parquetCompression.codec
	res.PagesMapBuf = make(map[string][]*layout.Page)
	res.DictRecs = make(map[string]*layout.DictRecType)
	res.NP = np
	res.Footer = parquet.NewFileMetaData()
	res.Footer.Version = 1
	res.Footer.Schema = append(res.Footer.Schema, res.SchemaHandler.SchemaElements...)
	res.Offset = 4
	_, err := res.PFile.Write([]byte("PAR1"))
	res.MarshalFunc = genMarshalTuples(schemaHandler)
	return res, err
}

// marshalTuples is the Parquet marshaler for the tuplesToRow() format.
func genMarshalTuples(pschema *schema.ParquetSchemaHandler) func([]interface{}, *pqschema.SchemaHandler) (*map[string]*layout.Table, error) {
	return func(records []interface{}, schemaHandler *pqschema.SchemaHandler) (*map[string]*layout.Table, error) {
		res := make(map[string]*layout.Table)
		if ln := len(records); ln <= 0 {
			return &res, nil
		}

		// Iterate over columns.
		for i := 0; i < len(records[0].([]interface{})); i++ {
			pathStr := schemaHandler.GetRootInName() + common.PAR_GO_PATH_DELIMITER + schemaHandler.Infos[i+1].InName
			table := layout.NewEmptyTable()
			res[pathStr] = table
			table.Path = common.StrToPath(pathStr)

			schema := schemaHandler.SchemaElements[schemaHandler.MapIndex[pathStr]]

			table.MaxDefinitionLevel, _ = schemaHandler.MaxDefinitionLevel(table.Path)
			table.MaxRepetitionLevel, _ = schemaHandler.MaxRepetitionLevel(table.Path)
			table.RepetitionType = schema.GetRepetitionType()
			table.Schema = schemaHandler.SchemaElements[schemaHandler.MapIndex[pathStr]]
			table.Info = schemaHandler.Infos[i+1]
			// Pre-allocate these arrays for efficiency
			table.Values = make([]interface{}, 0, len(records))
			table.RepetitionLevels = make([]int32, 0, len(records))
			table.DefinitionLevels = make([]int32, 0, len(records))

			// Iterate over records.
			var dl int32 = 1
			var rl int32 = 0
			for j := 0; j < len(records); j++ {
				val := records[j].([]interface{})[i]

				if val == nil {
					encodeNil(table, pschema.Zero(i), dl, rl)
				} else {
					encode(table, val, dl, rl)
				}
			}
		}
		return &res, nil
	}
}

func encode(table *layout.Table, value any, dl, rl int32) {
	switch tvalue := value.(type) {
	case []any:
		encodeSlice(table, tvalue, dl, rl)
	default:
		encodeValue(table, value, dl, rl)
	}
}

func encodeValue(table *layout.Table, value any, dl, rl int32) {
	//value = types.InterfaceToParquetType(value, table.Schema.Type)
	table.Values = append(table.Values, value)
	table.DefinitionLevels = append(table.DefinitionLevels, dl)
	table.RepetitionLevels = append(table.RepetitionLevels, rl)
}

func encodeNil(table *layout.Table, value any, dl, rl int32) {
	table.Values = append(table.Values, value)
	table.DefinitionLevels = append(table.DefinitionLevels, dl-1)
	table.RepetitionLevels = append(table.RepetitionLevels, rl)
}

func encodeSlice(table *layout.Table, l []any, dl, rl int32) {
	if len(l) == 0 {
		encode(table, nil, dl, rl)
		return
	}
	for i := 0; i < len(l); i++ {
		var nodeRL = rl + 1
		if i == 0 {
			nodeRL = rl
		}
		encodeValue(table, l[i], dl, nodeRL)
	}
}
