package pathgen

import (
	"bytes"
	crand "crypto/rand"
	"encoding/binary"
	"html/template"
	"io"
	"math/rand"
	"strings"
	"sync"
	"time"
)

var DefaultSuffix = ".{{.Now.Format \"200601021504\" }}.{{.Counter | printf \"%08x\"}}.{{.Random | printf \"%08x\"}}.pq"

type Generator struct {
	mx      sync.Mutex
	tpl     *template.Template
	counter int32
	rnd     *rand.Rand
}

func New(pattern string) (*Generator, error) {
	if !strings.Contains(pattern, "{{") {
		pattern += DefaultSuffix
	}

	tpl, err := template.New("").Parse(pattern)
	if err != nil {
		return nil, err
	}

	return &Generator{
		tpl: tpl,
		rnd: newRand(),
	}, nil
}

func (g *Generator) Next() (string, error) {
	g.mx.Lock()
	defer g.mx.Unlock()

	g.counter++
	vars := struct {
		Now     time.Time
		Counter int32
		Random  uint32
	}{
		Now:     time.Now(),
		Counter: g.counter,
		Random:  g.rnd.Uint32(),
	}

	var buf bytes.Buffer
	if err := g.tpl.Execute(&buf, vars); err != nil {
		return "", err
	}
	return buf.String(), nil
}

func newRand() *rand.Rand {
	var b [8]byte
	if _, err := io.ReadFull(crand.Reader, b[:]); err != nil {
		panic(err)
	}
	seed := int64(binary.BigEndian.Uint64(b[:]) & 0x7fffffffffffffff)
	return rand.New(rand.NewSource(seed))
}
