package writer

import (
	"errors"
	"net/url"
	"os"
	"path/filepath"
	"strings"

	"github.com/xitongsys/parquet-go-source/local"
	"github.com/xitongsys/parquet-go/source"
)

type sourceFactoryFunc func(*url.URL) (Source, error)

var sourceRegistry = map[string]sourceFactoryFunc{
	"local": func(uri *url.URL) (Source, error) {
		return NewLocalSource(uri), nil
	},
}

func registerSource(scheme string, f sourceFactoryFunc) {
	sourceRegistry[scheme] = f
}

type SourceURI struct {
	*url.URL
}

// ParseRemoteURI splits a URI into scheme and a path, checking that
// the scheme corresponds to a known Source.
func ParseRemoteURI(uri string) (*SourceURI, error) {
	u, err := url.Parse(uri)
	if err != nil {
		return nil, err
	}

	if u.Scheme == "" {
		u.Scheme = "local"
	}

	if _, ok := sourceRegistry[u.Scheme]; !ok {
		return nil, errUnknownSource
	}

	return &SourceURI{URL: u}, nil
}

func (u *SourceURI) Source() (Source, error) {
	gen := sourceRegistry[u.Scheme]
	return gen(u.URL)
}

// A Source is an abstraction over remote storage that extends
// parquet-go-source with the Rename operation, and provides a file
// creation factory function. It also provides hooks for the query
// layer to access those files when building its aggregate SQL view.
type Source interface {
	// Create a new file.
	Create(string) (source.ParquetFile, error)

	// Rename a file (old / new, like os.Rename).
	Rename(string, string) error

	// Return additional statements for database initialization.
	DatabaseInit() ([]string, error)

	// Return the Parquet URL for the remote glob according to the
	// DuckDB syntax (https://duckdb.org/docs/guides/import/s3_import.html).
	ParquetURI(string) string
}

// Local filesystem.
type localSource struct {
	uri *url.URL
}

func NewLocalSource(uri *url.URL) Source {
	return &localSource{uri: uri}
}

func (l *localSource) Create(path string) (source.ParquetFile, error) {
	return local.NewLocalFileWriter(path)
}

func (l *localSource) Rename(old, new string) error {
	return os.Rename(old, new)
}

func (l *localSource) DatabaseInit() ([]string, error) {
	return nil, nil
}

func (l *localSource) ParquetURI(glob string) string {
	return filepath.Join(l.uri.Path, glob)
}

var errUnknownSource = errors.New("unknown storage type")

func splitBucketAndPath(path string) (string, string, error) {
	n := strings.IndexByte(path, '/')
	if n < 0 {
		return "", "", errors.New("path is missing bucket")
	}
	bucket := path[:n]
	key := path[n+1:]
	return bucket, key, nil
}
