//go:build gcs

package writer

import (
	"context"
	"errors"
	"fmt"
	"net/url"

	"github.com/xitongsys/parquet-go-source/gcs"
	"github.com/xitongsys/parquet-go/source"
)

// GCS.
type gcsSource struct {
	projectID string
	uri       *url.URL
}

func newGCSSource(uri *url.URL) (Source, error) {
	return &gcsSource{
		projectID: uri.Host,
		uri:       uri,
	}, nil
}

func (s *gcsSource) Create(path string) (source.ParquetFile, error) {
	bucket, key, err := splitBucketAndPath(path)
	if err != nil {
		return nil, err
	}
	return gcs.NewGcsFileWriter(context.Background(), s.projectID, bucket, key)
}

func (s *gcsSource) Rename(old, new string) error {
	// TODO
	return errors.New("NOT IMPLEMENTED")
}

func (s *gcsSource) DatabaseInit() ([]string, error) {
	// TODO
	stmts := []string{
		"INSTALL httpfs",
		"LOAD httpfs",
		fmt.Sprintf("SET s3_endpoint = '%s'", s.uri.Host),
		fmt.Sprintf("SET s3_access_key_id = '%s'", ""),
		fmt.Sprintf("SET s3_secret_access_key = '%s'", ""),
	}

	return stmts, nil
}

func (s *gcsSource) ParquetURI(glob string) string {
	return fmt.Sprintf("s3://%s/%s", s.uri.Path, glob)
}

func init() {
	registerSource("gcs", newGCSSource)
}
