package writer

import (
	"context"
	"fmt"
	"net/url"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	miniosrc "github.com/xitongsys/parquet-go-source/minio"
	"github.com/xitongsys/parquet-go/source"
)

// Minio (generic S3) source.
type minioSource struct {
	uri    *url.URL
	client *minio.Client
	creds  *credentials.Credentials
}

func newMinioSource(uri *url.URL) (Source, error) {
	creds := credentials.NewEnvMinio()

	client, err := minio.New(uri.Host, &minio.Options{
		Creds:  creds,
		Secure: true,
	})
	if err != nil {
		return nil, err
	}

	return &minioSource{
		uri:    uri,
		client: client,
		creds:  creds,
	}, nil
}

func (s *minioSource) Create(path string) (source.ParquetFile, error) {
	bucket, key, err := splitBucketAndPath(path)
	if err != nil {
		return nil, err
	}

	return miniosrc.NewS3FileWriterWithClient(context.Background(), s.client, bucket, key)
}

func (s *minioSource) Rename(old, new string) error {
	oldBucket, oldKey, err := splitBucketAndPath(old)
	if err != nil {
		return err
	}

	newBucket, newKey, err := splitBucketAndPath(new)
	if err != nil {
		return err
	}

	// Copy old to new.
	_, err = s.client.CopyObject(
		context.Background(),
		minio.CopyDestOptions{
			Bucket: newBucket,
			Object: newKey,
		},
		minio.CopySrcOptions{
			Bucket: oldBucket,
			Object: oldKey,
		},
	)
	if err != nil {
		return err
	}

	// Remove old.
	return s.client.RemoveObject(
		context.Background(),
		oldBucket,
		oldKey,
		minio.RemoveObjectOptions{},
	)
}

func (s *minioSource) DatabaseInit() ([]string, error) {
	creds, err := s.creds.Get()
	if err != nil {
		return nil, err
	}

	stmts := []string{
		"INSTALL httpfs",
		"LOAD httpfs",
		fmt.Sprintf("SET s3_endpoint = '%s'", s.uri.Host),
	}
	if creds.SessionToken != "" {
		stmts = append(stmts,
			fmt.Sprintf("SET s3_session_token = '%s'", creds.SessionToken))
	} else {
		stmts = append(stmts,
			fmt.Sprintf("SET s3_access_key_id = '%s'", creds.AccessKeyID),
			fmt.Sprintf("SET s3_secret_access_key = '%s'", creds.SecretAccessKey))
	}

	return stmts, nil
}

func (s *minioSource) ParquetURI(glob string) string {
	return fmt.Sprintf("s3://%s/%s", s.uri.Path, glob)
}

func init() {
	registerSource("minio", newMinioSource)
}
