//go:build s3

package writer

import (
	"context"
	"errors"
	"fmt"
	"net/url"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/xitongsys/parquet-go-source/s3v2"
	"github.com/xitongsys/parquet-go/source"
)

// S3.
type s3Source struct {
	cfg    aws.Config
	client *s3.Client
	bucket string
	uri    *url.URL
}

func newS3Source(uri *url.URL) (Source, error) {
	cfg, err := config.LoadDefaultConfig(context.Background())
	if err != nil {
		return nil, err
	}

	return &s3Source{
		cfg:    cfg,
		client: s3.NewFromConfig(cfg),
		bucket: uri.Host,
		uri:    uri,
	}, nil
}

func (s *s3Source) Create(path string) (source.ParquetFile, error) {
	return s3v2.NewS3FileWriter(context.Background(), s.bucket, path, nil, &s.cfg)
}

func (s *s3Source) Rename(old, new string) error {
	// TODO
	return errors.New("NOT IMPLEMENTED")
}

func (s *s3Source) DatabaseInit() ([]string, error) {
	// TODO
	stmts := []string{
		"INSTALL httpfs",
		"LOAD httpfs",
		fmt.Sprintf("SET s3_endpoint = '%s'", s.uri.Host),
		fmt.Sprintf("SET s3_access_key_id = '%s'", ""),
		fmt.Sprintf("SET s3_secret_access_key = '%s'", ""),
	}

	return stmts, nil
}

func (s *s3Source) ParquetURI(glob string) string {
	return fmt.Sprintf("s3://%s/%s", s.uri.Path, glob)
}

func init() {
	registerSource("s3", newS3Source)
}
