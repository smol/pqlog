package writer

import (
	"flag"
	"fmt"
	"log"
	"time"

	"git.autistici.org/smol/pqlog/schema"
	"git.autistici.org/smol/pqlog/writer/pathgen"
	"github.com/xitongsys/parquet-go/source"
	pqwriter "github.com/xitongsys/parquet-go/writer"
)

var parquetConcurrency = flag.Int("parquet-concurrency", 2, "concurrency of the Parquet writer")

// Writer is the interface for our restartable ParquetWriter.
type Writer interface {
	Reopen(*schema.ParquetSchemaHandler) error
	Write(any) error
	Close() error
}

// The ParquetWriter writes a sequence of sequentially numbered
// Parquet files, switching to a new one whenever the schema changes.
// It guarantees the creation of consistent Parquet files.
type ParquetWriter struct {
	pg  *pathgen.Generator
	src Source

	lw       source.ParquetFile
	pw       *pqwriter.ParquetWriter
	renameTo string
}

func New(src Source, pg *pathgen.Generator) *ParquetWriter {
	return &ParquetWriter{
		src: src,
		pg:  pg,
	}
}

func (w *ParquetWriter) Write(obj any) error {
	return w.pw.Write(obj)
}

func (w *ParquetWriter) Close() error {
	var err error
	if w.pw != nil {
		err = w.pw.WriteStop()
		w.pw = nil
	}
	if w.lw != nil {
		w.lw.Close()
		w.lw = nil
	}
	if w.renameTo != "" {
		// Only rename the file if WriteStop was successful.
		if err == nil {
			err = w.src.Rename(w.renameTo+".tmp", w.renameTo)
		}
		w.renameTo = ""
	}
	return err
}

// (Re)open the output with a new schema.
func (w *ParquetWriter) Reopen(schemaHandler *schema.ParquetSchemaHandler) error {
	if err := w.Close(); err != nil {
		log.Printf("error finalizing Parquet file: %v", err)
	}

	path, err := w.pg.Next()
	if err != nil {
		return err
	}

	log.Printf("creating parquet file %s (%d fields)", path, len(schemaHandler.Infos)-1)

	w.lw, err = w.src.Create(path + ".tmp")
	if err != nil {
		return fmt.Errorf("can't create Parquet file: %w", err)
	}
	w.renameTo = path

	w.pw, err = newTuplesWriter(schemaHandler, w.lw, int64(*parquetConcurrency))
	if err != nil {
		return fmt.Errorf("can't create Parquet writer: %w", err)
	}
	return nil
}

// Maximum number of records written to any Parquet file before rotating it.
var MaxRecordsPerFile int64 = 1000000

// A RotatingParquetWriter will rotate the output file on a specific period.
type RotatingParquetWriter struct {
	*ParquetWriter

	counter       int64
	period        time.Duration
	deadline      time.Time
	schemaHandler *schema.ParquetSchemaHandler
}

func NewRotating(src Source, pg *pathgen.Generator, period time.Duration) *RotatingParquetWriter {
	return &RotatingParquetWriter{
		ParquetWriter: New(src, pg),
		period:        period,
	}
}

func (w *RotatingParquetWriter) Write(obj any) error {
	w.counter++

	if time.Now().After(w.deadline) || w.counter > MaxRecordsPerFile {
		if err := w.Reopen(w.schemaHandler); err != nil {
			return err
		}
	}

	return w.ParquetWriter.Write(obj)
}

func (w *RotatingParquetWriter) Reopen(schemaHandler *schema.ParquetSchemaHandler) error {
	if err := w.ParquetWriter.Reopen(schemaHandler); err != nil {
		return err
	}

	w.schemaHandler = schemaHandler
	w.counter = 0

	// Round to period for more accurate alignment.
	w.deadline = time.Now().Add(w.period).Round(w.period)

	return nil
}
